#include <QDebug>
#include <QFile>
#include <iostream>
#include <QPoint>
#include <QString>
/*QDebug(const QDebug &o)
QDebug(QtMsgType t)
QDebug(QString *string)
QDebug(QIODevice *device)*/

int main()
{
   QFile file("out.txt");
   if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
           std::cout << "cannot open file" << std::endl;
           exit(EXIT_FAILURE);
       }

       QDebug debug(&file);

       debug << "Orhan OZTURK" << 999;

       qDebug() << "debug.autoInsertSpaces : " << debug.autoInsertSpaces();
       debug.setAutoInsertSpaces(false);
       qDebug() << "debug.autoInsertSpaces : " << debug.autoInsertSpaces();
       debug.setAutoInsertSpaces(true);
       debug << "111111111";
       debug << "222222222";
       debug.setAutoInsertSpaces(false);
       debug << "333333333";
       debug << "444444444";

       qDebug() << "verbosity : " << debug.verbosity();
           debug.setVerbosity(7);
           qDebug() << "verbosity : " << debug.verbosity();
           debug.resetFormat();
           qDebug() << "debug.autoInsertSpaces : " << debug.autoInsertSpaces();
           qDebug() << "verbosity : " << debug.verbosity();
           qDebug() << "**************************************************";

           int a = 5, b = 10;
           qDebug() << "a: " << a << "b : " << b; // "a: 5 b: 10"
           qDebug().nospace() << "a: " << a << "b : " << b; // a: 5b: 10

           qDebug().maybeSpace() << a << b;
           qDebug().maybeQuote() << a << b;

           QString str("ece");
           qDebug().quote() << str;
           qDebug().noquote() << str;
           qDebug().nospace() << a << b;

          QPoint point(5,10);
          qDebug() << point;


    return 0;
}
