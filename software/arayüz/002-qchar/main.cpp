
#include<QDebug>
int main()
{
    /*
    QChar(uchar ch)
    QChar(char ch)
    QChar(wchar_t ch)
    QChar(char16_t ch)
    QChar(QLatin1Char ch)
    QChar(QChar::SpecialCharacter ch)
    QChar(int code)
    QChar(uint code)
    QChar(short code)
    QChar(uchar cell, uchar row)
    QChar(ushort code)
    QChar()*/
    {
       QChar ch1('a');
       qDebug() << "ch1 :" << ch1;
    }
   // isNumber: Returns true if the character is a number (Number_* categories, not just 0-9); otherwise returns false.
      QChar ch5 ('6');
      qDebug() << "ch5:" << ch5.isNumber();

      QChar ch6 ('-6');
      qDebug() << "ch6:" << ch6.isNumber();
   // isNull:Returns true if the character is the Unicode character 0x0000 ('\0'); otherwise returns false.

      QChar ch7('z');
      QChar ch8('\0');
      qDebug() << "ch7:" <<ch7;
      qDebug() << "ch8:" <<ch8;
      qDebug() << "ch7:" << ch7.isNull();
      qDebug() << "ch8:" << ch8.isNull();

    //toLower:
    //toUpper:Returns the uppercase equivalent if the character is lowercase or titlecase; otherwise returns the character itself.

      QChar ch9('a');
      qDebug() <<"ch9:" <<ch9.toLower();
      qDebug() <<"ch9:" <<ch9.toUpper();
      qDebug() <<"ch9:" <<ch9.isLower();
      qDebug() << QChar::isLower(122);
    return 0;
}
