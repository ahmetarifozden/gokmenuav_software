#include <QString>
#include<QDebug>

/*
QString(const QByteArray &ba)
QString(const char *str)
QString(QString &&other)
QString(const QString &other)
QString(QLatin1String str)
QString(int size, QChar ch)
QString(QChar ch)
QString(const QChar *unicode, int size = -1)
QString()
*/
int main()
{


    QString str;
    QString csv = "forename,middlename,surname,phone";
    QString path = "/usr/local/bin/myapp"; // First field is empty
    QString::SectionFlag flag = QString::SectionSkipEmpty;

    str = csv.section(',', 2, 2);   // str == "surname"
    str = path.section('/', 3, 4);  // str == "bin/myapp"
    str = path.section('/', 3, 3, flag); // str == "myapp"



    return 0;
}
