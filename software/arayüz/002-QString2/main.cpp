#include <QChar>
#include <QDebug>
#include <string>
#include <iostream>
int main()
{
    //numberstatic:
    {
      long a = 63;
      QString s = QString::number(a, 16);
      qDebug() << "s:" << QString::number(a,16); // s == "3f"
      QString t = QString::number(a, 16).toUpper();     // t == "3F"
      qDebug() << "t:" << QString::number(a, 16).toUpper();
    }
    //toInt,toDouble,toLong
    {
    QString str("234.45");
    double result;
    result = str.toDouble();
    qDebug() << "result:" << result;
    }
    {
    QString str("234");
    int result1;
    result1 = str.toInt();
    qDebug() << "result1:" << result1;
    }
    {
        QString str("234");
        int result2;
        result2 = str.toLong();
        qDebug() << "result2:" << result2;
    }
    //split
    {
        QString str("a,b,c");
        QStringList list1= str.split(',');
        qDebug() << list1;

        foreach(QString item,list1){
            qDebug() << item;
        }
    }
    // arg %n
    {
        int a =5, b=10;
        QString str("a= %1,b= %2");
        qDebug() << str;
    }
    return 0;
}
